package com.example.lab4vovanlinh;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainConectivity extends AppCompatActivity {
    private static final int REQUEST_PERMISSIONS = 100;
    private static final String DEBUG_TAG = "NetworkStatusExample";
    Button btninternet;
    TextView txtinternet;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_conectivity);
        request();
        anhxa();
        onclick();
    }

    private void onclick() {
    btninternet.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            onclickinternet();
        }
    });
    }

    private void anhxa() {
    btninternet=findViewById(R.id.btninternet);
    txtinternet=findViewById(R.id.txtinternet);
    }
    private void onclickinternet(){
        ConnectivityManager connMgr =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
       NetworkInfo networkInfo=connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
       boolean isWifiConn=networkInfo.isConnected();
       networkInfo=connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
       boolean isMobileConn=networkInfo.isConnected();
       if (isWifiConn){
txtinternet.setText("Wifi Connection");
           //Toast.makeText(this,"Wifi connection",Toast.LENGTH_SHORT).show();
       }
        else if (isMobileConn){
          txtinternet.setText("Mobile Connection");
           //Toast.makeText(this,"Mobile connection",Toast.LENGTH_SHORT).show();
        }
        else {
           txtinternet.setText("Not Connection");
           //Toast.makeText(this,"Not connection",Toast.LENGTH_SHORT).show();
       }
    }

    private void request(){
        if ((ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.ACCESS_NETWORK_STATE) != PackageManager.PERMISSION_GRANTED)) {
            if ((ActivityCompat.shouldShowRequestPermissionRationale(MainConectivity.this,
                    Manifest.permission.INTERNET)) && (ActivityCompat.shouldShowRequestPermissionRationale(MainConectivity.this,
                    Manifest.permission.ACCESS_NETWORK_STATE))) {

            } else {
                ActivityCompat.requestPermissions(MainConectivity.this,
                        new String[]{Manifest.permission.INTERNET, Manifest.permission.ACCESS_NETWORK_STATE},
                        REQUEST_PERMISSIONS);
            }
        }else {

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSIONS: {
                for (int i = 0; i < grantResults.length; i++) {
                    if (grantResults.length > 0 && grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    } else {
                        Toast.makeText(MainConectivity.this,
                                "The app was not allowed to read or write to your storage. Hence, it cannot function properly. Please consider granting it this permission", Toast.LENGTH_LONG).show();
                    }
                }
            }
        }
    }
}
