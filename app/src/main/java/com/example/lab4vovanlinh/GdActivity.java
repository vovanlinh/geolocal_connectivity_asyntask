package com.example.lab4vovanlinh;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class GdActivity extends AppCompatActivity {
    Button btnb1,btnb2,btnb3,btnb3b;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gd);
        anhxa();
        setonClick();
    }
    private void setonClick(){
        btnb1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(GdActivity.this,MainActivity.class);
                startActivity(intent);
            }
        });
        btnb2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(GdActivity.this,MainConectivity.class);
                startActivity(intent);
            }
        });
        btnb3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(GdActivity.this,DownloadActivity.class);
                startActivity(intent);
            }
        });
        btnb3b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(GdActivity.this,LoadChuoi.class);
                startActivity(intent);
            }
        });
    }
    private void anhxa() {
        btnb1=findViewById(R.id.btnbai1);
        btnb2=findViewById(R.id.btnbai2);
        btnb3=findViewById(R.id.btnbai3);
        btnb3b=findViewById(R.id.btnbai3b);
    }
}