package com.example.lab4vovanlinh;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class DownloadActivity extends AppCompatActivity {
Button btnload;
ImageView imgfile;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download);
    anhxa();
    onclick();
    }

    private void onclick() {
        btnload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new LoadImageInternet().execute("https://codinginfinite.com/wp-content/uploads/2018/05/Download-Manager-Apps-For-Android.jpg");
            }
        });
    }

    private void anhxa() {
    btnload=findViewById(R.id.btnload);
    imgfile=findViewById(R.id.imgfile);
    }
    private class LoadImageInternet extends AsyncTask<String,Void, Bitmap>{
        Bitmap bitmapHinh=null;
        @Override
        protected Bitmap doInBackground(String... strings) {
            try {
                URL url=new URL(strings[0]);
                InputStream inputStream=url.openConnection().getInputStream();
                bitmapHinh=BitmapFactory.decodeStream(inputStream);

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bitmapHinh;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            imgfile.setImageBitmap(bitmap);
        }
    }
}
