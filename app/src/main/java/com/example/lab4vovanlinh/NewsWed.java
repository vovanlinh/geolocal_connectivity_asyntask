package com.example.lab4vovanlinh;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

public class NewsWed extends AppCompatActivity {
WebView wb;
Button btnback;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_wed);
        wb=findViewById(R.id.wedviewnews);
        btnback=findViewById(R.id.btnwedback);
        Intent intent=getIntent();
        String link=intent.getStringExtra("linktintuc");
        wb.loadUrl(link);
        wb.setWebViewClient(new WebViewClient());
        btnback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }
}
