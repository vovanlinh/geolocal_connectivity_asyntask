package com.example.lab4vovanlinh;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

public class LoadChuoi extends AppCompatActivity {

    ListView lv;
    ArrayList<String> arrayTitle,arrayLink;
    ArrayAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_chuoi);
    anhxa();
        arrayTitle=new ArrayList<>();
        arrayLink=new ArrayList<>();
        new ReadWedsite().execute("https://vnexpress.net/rss/suc-khoe.rss");
    adapter=new ArrayAdapter(this,android.R.layout.simple_list_item_1,arrayTitle);
    lv.setAdapter(adapter);

    lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            Intent intent=new Intent(LoadChuoi.this,NewsWed.class);
            intent.putExtra("linktintuc",arrayLink.get(i));
            startActivity(intent);
        }
    });
    }




    private void anhxa() {
   lv=findViewById(R.id.listviewwb);

    }
    private class ReadWedsite extends AsyncTask<String,Void,String>{
        @Override
        protected String doInBackground(String... strings) {
            StringBuilder content=new StringBuilder();
            try {
                URL url=new URL(strings[0]);
                InputStreamReader inputStreamReader=new InputStreamReader(url.openConnection().getInputStream());
                BufferedReader bufferedReader=new BufferedReader(inputStreamReader);
                String line="";
while ((line=bufferedReader.readLine())!=null){
    content.append(line);
}
bufferedReader.close();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return content.toString();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
XMLDOMParser parser =new XMLDOMParser();
            Document document=parser.getDocument(s);
            NodeList nodeList=document.getElementsByTagName("item");
            String tieude="";
            for (int i=0;i<nodeList.getLength();i++){
                Element element= (Element) nodeList.item(i);
                tieude=parser.getValue(element,"title")+"\n";
                arrayTitle.add(tieude);
                arrayLink.add(parser.getValue(element,"link"));
            }
            adapter.notifyDataSetChanged();
        }
    }
}
